Goal
----
Show the books in a grid rather than tables

Walk through
------------
Explain that Bootstrap3 is always 100% stacked unless you add breakpoints for the given screensizes
Show breakpoints: http://getbootstrap.com/css/#grid-options

Start with breakpoints for large (or medium devices depending on beamer resolution)
Then add a breakpoint for smaller screens

show window resizer
show chrome user agent settings


Test
----
http://localhost:63342/SciFiShop/5-the-grid/index.html